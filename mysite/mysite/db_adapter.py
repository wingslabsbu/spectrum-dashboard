# Developers : Ayon Chakraborty

from pymongo import *
from bson.json_util import dumps
from helpers import mac_encode, UserEquipment, UE_Measurement
import statsmodels.api as sm
import numpy as np
import time

HOSTNAME = '130.245.144.191'
PORT = 27017
DBNAME = 'SpecsenseDB'

def getReadingsinRadius1(lat=0, lon=0, radius=0):

    old = time.time() * 1000

    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    ue_registered = db['Registered_UE']


    #  Within Sphere :
    # Earth Radian factor = 3959
    # Might need to run db.getCollection('UE_Measurements').ensureIndex({point:"2dsphere"}); on db
    # Also run - db.getCollection('UE_Measurements').createIndex( { loc : "2dsphere" } );
    r = float(radius) / 3959.0
    dataOutput = list(ue_measurements.find({'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}}))

    print "DB query TOOK: "+str( time.time() * 1000 - old )
    old = time.time() * 1000

    channel_powers = {}
    channel_occupancies = {}
    macs = []

    for doc in dataOutput:
        if doc["mac"] not in macs:
            macs.append(doc["mac"])
        channel = str(doc["ue_channel_scanned"])
        if channel in channel_powers.keys():
            channel_powers[channel].append(float(doc["totalpower"]))
            if float(doc["pilot"]) == 1.0:
                channel_occupancies[channel]["occupied"] += 1
            else:
                channel_occupancies[channel]["whitespace"] += 1 #inefficient but convinient coding
        else:
            channel_powers[channel] = [float(doc["totalpower"])]
            channel_occupancies[channel] = {"occupied": 0, "whitespace": 0}
            if float(doc["pilot"]) == 1.0:
                channel_occupancies[channel]["occupied"] += 1
            else:
                channel_occupancies[channel]["whitespace"] += 1 #inefficient but convinient coding


    print "Occupancy computation: "+str( time.time() * 1000 - old ) #This is very high, needs urgent optimization
    old = time.time() * 1000


    for channel in channel_powers.keys():
        channelpowerdata = channel_powers[channel]
        x = np.linspace(-5,15,100)
        ecdf = sm.distributions.ECDF(np.array(channelpowerdata))
        cdfdata = ecdf(x)
        channel_powers[channel] = list(cdfdata)

    ue_registered_output = ue_registered.find({'_id': {'$in': macs}})
    registered_sensor_data = []
    for doc in ue_registered_output:
        registered_sensor_data.append({"mac": doc["_id"], "ue_status": doc["ue_status"], "seen": doc["seen"], "battery": doc["ue_battery_power"]})

    finalData = {'registered_sensors': registered_sensor_data, 'powers': channel_powers, 'occupancycharts': channel_occupancies }
    print "CDF FUNCTION TOOK: "+str( time.time() * 1000 - old )

    return dumps(finalData)

def getReadingsinRadius(lat=0, lon=0, radius=0):

    old = time.time() * 1000

    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    ue_registered = db['Registered_UE']

    # Within Sphere :
    # Earth Radian factor = 3959
    # Might need to run db.getCollection('UE_Measurements').ensureIndex({point:"2dsphere"}); on db
    # Also run - db.getCollection('UE_Measurements').createIndex( { loc : "2dsphere" } );
    r = float(radius) / 3959.0
    #dataOutput = ue_measurements.find({'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}})

    #print "DB query TOOK: "+str( time.time() * 1000 - old )
    old = time.time() * 1000

    channel_powers = {}
    channel_occupancies = {}
    macs = []
    allchannels = range(14,52)
    allchannels.append(61)

    # for channel in allchannels:
    #     channel = str(channel)
    #     channel_occupancies[channel] = {"occupied": 0, "whitespace": 0}
    #
    #     channel_occupancies[channel]["occupied"] = ue_measurements.find({
    #                                             '$and': [
    #                                                         {'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},
    #                                                         {'ue_channel_scanned':channel},
    #                                                         {'pilot':"1"}
    #                                                     ]
    #                                                 }).count()
    #
    #     channel_occupancies[channel]["whitespace"] = ue_measurements.find({
    #                                             '$and': [
    #                                                         {'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},
    #                                                         {'ue_channel_scanned':channel},
    #                                                         {'pilot':"0"}
    #                                                     ]
    #                                                 }).count()
    #
    #     channel_powers[channel] = ue_measurements.distinct('totalpower', {
    #                                             '$and': [
    #                                                         {'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},
    #                                                         {'ue_channel_scanned':channel}
    #                                                     ]
    #                                                 }) #MOST BOGUS WAY OF DOING :( .. NEED TO KNOW HOW TO GET A LIST


    channel_powers = list( db.UE_Measurements.aggregate( [{'$match': {'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}}},{ "$group" : { "_id": "$ue_channel_scanned", "powers" : {"$push" : "$totalpower"}}}]))
    print "Channel power time: " + str(time.time()*1000 - old)
    old = time.time()*1000
    
    occupiedChannels = list(db.UE_Measurements.aggregate( [ { "$match" : { "$and": [{'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},{ "pilot": "1"}] }}, { "$group" : {"_id": "$ue_channel_scanned" , "total": {"$sum" : 1}}} ]))
    unoccupiedChannels = list(db.UE_Measurements.aggregate( [ { "$match" : { "$and": [{'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},{ "pilot": "0"}] }}, { "$group" : {"_id": "$ue_channel_scanned" , "total": {"$sum" : 1}}} ]))

    channel_occupancies = {}
    for channel in allchannels:
        channel = str(channel)
        channel_occupancies[channel] = {"occupied": 0, "whitespace": 0}

    for occupiedChannel in occupiedChannels:
        #if occupiedChannel['_id'] not in '61':
        channel_occupancies[occupiedChannel['_id']]['occupied'] = channel_occupancies[occupiedChannel['_id']]['occupied'] + occupiedChannel['total']

    for unoccupiedChannel in unoccupiedChannels:
        #if unoccupiedChannel['_id'] not in '61':
        channel_occupancies[unoccupiedChannel['_id']]['whitespace'] = channel_occupancies[unoccupiedChannel['_id']]['whitespace'] + unoccupiedChannel['total']

    print "Occupancy computation: "+str( time.time() * 1000 - old ) #This is very high, needs urgent optimization
    old = time.time() * 1000

    ch_pow = {}
    for channel in channel_powers:
        channelpowerdata =  [float(x) for x in channel['powers']]
        x = np.linspace(-5,15,100)
        ecdf = sm.distributions.ECDF(np.array(channelpowerdata))
        cdfdata = ecdf(x)
        ch_pow[channel['_id']] = list(cdfdata)

    macs = list(ue_registered.find({'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},{'mac':1}))
    macs = [str(x['_id']) for x in macs]
    ue_registered_output = ue_registered.find({'_id': {'$in': macs}})
    registered_sensor_data = []
    for doc in ue_registered_output:
        registered_sensor_data.append({"mac": doc["_id"], "ue_status": doc["ue_status"], "seen": doc["seen"], "battery": doc["ue_battery_power"]})

    finalData = {'registered_sensors': registered_sensor_data, 'powers': ch_pow, 'occupancycharts': channel_occupancies }
    print "CDF FUNCTION TOOK: "+str( time.time() * 1000 - old )

    return dumps(finalData)

def get_channel_occupancy():
    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    return occupancycharts

def getChannels():
    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    dataOutput = ue_measurements.distinct('ue_channel_scanned')
    results = [int(i) for i in dataOutput]
    results.sort()
    return results

def getUEMeasurements(channel):
    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    dataOutput = ue_measurements.find({'ue_channel_scanned': channel})
    return dumps(dataOutput)

def populate_ue_from_db_entry(ue, entry, ue_measurements):
    ue.id = mac_encode(entry["_id"])
    ue.mac = entry["_id"]
    ue.model = entry["ue_model"]
    ue.status = entry["ue_status"]
    ue.total_measurements = ue_measurements.find({'mac': ue.mac}).count()
    ue_meas = ue_measurements.find({'mac': ue.mac}).sort("last_scanned", DESCENDING).limit(1)
    if ue_meas.count() > 0:
        measurement = ue_meas[0]
        ue.last_latitude = measurement["loc"]["coordinates"][1]
        ue.last_longitude = measurement["loc"]["coordinates"][0]
        ue.last_scantime = measurement["last_scanned"]
    else:
        ue.last_latitude = "N/A"
        ue.last_longitude = "N/A"
        ue.last_scantime = "N/A"

def getAllRegisteredUEData():
    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    ue_registered = db['Registered_UE']
    all_ue_registered = ue_registered.find().sort('ue_status', DESCENDING)

    finalData = []
    for entry in all_ue_registered:
        ue = UserEquipment()
        populate_ue_from_db_entry(ue, entry, ue_measurements)
        finalData.append(ue)
    return finalData

def findUEFromMac(mac):
    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    ue_registered = db['Registered_UE']
    result = ue_registered.find({"_id": mac}).limit(1)
    if result.count() > 0:
        entry = result[0]
    else:
        return None
    ue = UserEquipment()
    populate_ue_from_db_entry(ue, entry, ue_measurements)
    # all_measurements = ue_measurements.find({"mac" : ue.mac}).sort("last_scanned", ASCENDING)
    all_measurements = ue_measurements.find({"mac" : ue.mac}).sort("last_scanned", DESCENDING).limit(20).sort("last_scanned", ASCENDING)
    for measurement_entry in all_measurements:
        measurement = UE_Measurement()
        measurement.battery_level = measurement_entry["ue_battery_power"]
        measurement.channel_power = measurement_entry["totalpower"]
        measurement.channel = measurement_entry["ue_channel_scanned"]
        measurement.latitude = measurement_entry["loc"]["coordinates"][1]
        measurement.longitude = measurement_entry["loc"]["coordinates"][0]
        measurement.scan_time = measurement_entry["last_scanned"]
        ue.measurements.append(measurement)
    return ue
