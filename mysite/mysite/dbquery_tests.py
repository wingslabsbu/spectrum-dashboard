# Developers : Ayon Chakraborty

from pymongo import *
#from bson.json_util import dumps
#from helpers import mac_encode, UserEquipment, UE_Measurement
import time
import statsmodels.api as sm
import numpy as np

HOSTNAME = '130.245.144.191'
PORT = 27017
DBNAME = 'SpecsenseDB'

def getReadingsinRadius(lat=0, lon=0, radius=0):

    lat = 40.9256538
    lon = -73.14094290000003
    radius = 5

    old = time.time() * 1000

    client = MongoClient(HOSTNAME, PORT)
    db = client[DBNAME]
    ue_measurements = db['UE_Measurements']
    ue_registered = db['Registered_UE']

    #  Within Sphere :
    # Earth Radian factor = 3959
    # Might need to run db.getCollection('UE_Measurements').ensureIndex({point:"2dsphere"}); on db
    # Also run - db.getCollection('UE_Measurements').createIndex( { loc : "2dsphere" } );
    r = float(radius) / 3959.0
    dataOutput = ue_measurements.find({'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}})

    print "DB query TOOK: "+str( time.time() * 1000 - old )
    old = time.time() * 1000

    channel_occupancies = {}
    channel_powers = {}
    for channel in range(14,52):
        channel = str(channel)
        channel_occupancies[channel] = {"occupied": 0, "whitespace": 0}

        channel_occupancies[channel]["occupied"] = ue_measurements.find({
                                                '$and': [
                                                            {'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},
                                                            {'ue_channel_scanned':channel},
                                                            {'pilot':"1"}
                                                        ]
                                                    }).count()
        totalcount = ue_measurements.find({
                                                '$and': [
                                                            {'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},
                                                            {'ue_channel_scanned':channel}
                                                        ]
                                                    }).count()
        channel_occupancies[channel]["whitespace"] = totalcount - channel_occupancies[channel]["occupied"]

        channel_powers[channel] = ue_measurements.find({
                                                '$and': [
                                                            {'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},
                                                            {'ue_channel_scanned':channel}
                                                        ]
                                                    },
                                                {'totalpower':1,'_id':0})
        print channel, channel_occupancies[channel]

    macs = ue_registered.find({'loc': {'$geoWithin':{'$centerSphere': [[lon, lat], r]}}},{'_id':1})
    macs = [str(x['_id']) for x in macs]

    ##################################################################################
    # channel_powers = {}
    # channel_occupancies = {}
    # macs = []
    #
    # for doc in dataOutput:
    #     if doc["mac"] not in macs:
    #         macs.append(doc["mac"])
    #     channel = str(doc["ue_channel_scanned"])
    #     if channel in channel_powers.keys():
    #         channel_powers[channel].append(float(doc["totalpower"]))
    #         if float(doc["pilot"]) == 1.0:
    #             channel_occupancies[channel]["occupied"] += 1
    #         else:
    #             channel_occupancies[channel]["whitespace"] += 1 #inefficient but convinient coding, inefficient as later .. total - occupied = whitespace
    #     else:
    #         channel_powers[channel] = [float(doc["totalpower"])]
    #         channel_occupancies[channel] = {"occupied": 0, "whitespace": 0}
    #         if float(doc["pilot"]) == 1.0:
    #             channel_occupancies[channel]["occupied"] += 1
    #         else:
    #             channel_occupancies[channel]["whitespace"] += 1 #inefficient but convinient coding


    print "Occupancy computation: "+str( time.time() * 1000 - old ) #This is very high, needs URGENT OPTIMIZATION
    old = time.time() * 1000
    ###################################################################################

    for channel in range(14,52):
        channel = str(channel)
        channelpowerdata =  [float(x['totalpower']) for x in channel_powers[channel]]
        x = np.linspace(-5,15,100)
        ecdf = sm.distributions.ECDF(np.array(channelpowerdata))
        cdfdata = ecdf(x)
        channel_powers[channel] = list(cdfdata)

    ue_registered_output = ue_registered.find({'_id': {'$in': macs}})
    registered_sensor_data = []
    for doc in ue_registered_output:
        registered_sensor_data.append({"mac": doc["_id"], "ue_status": doc["ue_status"]})

    finalData = {'registered_sensors': registered_sensor_data, 'powers': channel_powers, 'occupancycharts': channel_occupancies }
    print "CDF FUNCTION TOOK: "+str( time.time() * 1000 - old )




getReadingsinRadius(0,0,0)
